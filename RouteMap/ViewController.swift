//
//  ViewController.swift
//  RouteMap
//
//  Created by LeeFu on 11/29/17.
//  Copyright © 2017 Lor LeeFu. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class ViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {

    var mapView = GMSMapView()
    
    var long: CLLocationDegrees!
    var lat: CLLocationDegrees!
    
    var locationManager = CLLocationManager()
    var yourLocation = CLLocation()
    var locationFix = CLLocation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lat = 43.696710
        long = -1.067496
        locationFix = CLLocation(latitude: lat, longitude: long)
        
        //Initail code to get Access to your location and get date
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startMonitoringSignificantLocationChanges()
        
        //Initial mapView-camera-marker
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 14.0)
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = mapView
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude:  long)
        marker.title = "Dax"
        marker.snippet = "France"
        marker.map = mapView
        
        //Create button that allow u to find ur locatin
        mapView.delegate = self
        mapView.isMyLocationEnabled = false
        mapView.settings.myLocationButton = true
        mapView.settings.compassButton = true
        mapView.settings.zoomGestures = true
        
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error to get location : \(error)")
    }
    
    //This func will get ur last update location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        yourLocation = locations.last!
        self.locationManager.stopUpdatingLocation()
    }
    
    //This func will allow u to show u location when start app
//    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
//        mapView.isMyLocationEnabled = true
//    }
    
    // Set action to button myLocation
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        mapView.isMyLocationEnabled = true
        
        // Draw route -> see detail in GoogleRouteService.swfit
        GoogleRouteService.shareInstance.drawPath(startLocation: yourLocation, endLocation: locationFix, googleMaps: mapView)
        
        mapView.selectedMarker = nil
        return false
    }
}

